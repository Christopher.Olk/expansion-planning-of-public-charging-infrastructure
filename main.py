import numpy as np
import matplotlib.pyplot as plt
from scipy.spatial import Voronoi, voronoi_plot_2d
from scipy.interpolate import interp2d
from matplotlib import path
import math
import matplotlib.colors
import matplotlib.patches as mpatches
from geneticalgorithm import geneticalgorithm as ga
from scipy import interpolate
import smopy
import socket
import multiprocessing
import json
import os
import glob
import pandas as pd
import warnings

seed = 42
np.random.seed(seed)
radius_relevant = 0.01
bottom_left = (0, 0)  # (x, y)
top_right = (10, 10)  # (x, y)
steps = 1000
coords_hm = None
indices_hm = None

debug_mode = False
plot_wedges = False

data_path = '/home/col/Documents/GIS/New_site_suggestor/Heatmaps/'


def make_cmap():
    """
    Creates a custom color map that reduces the alpha-value for low values and thereby effectively becoming transparent. This is necessary to ensure that the map under the heatmap can also be seen
    :return: The created heat map
    """
    cmap = plt.cm.hot
    # Get the colormap colors
    my_cmap = cmap(np.arange(cmap.N))
    # Set alpha
    alphas = np.ones(cmap.N)
    non_alpha_cnt = int(cmap.N * 0.6)
    alphas[:(cmap.N - non_alpha_cnt)] = np.linspace(0, 1, cmap.N - non_alpha_cnt)
    my_cmap[:, -1] = alphas
    my_cmap = matplotlib.colors.LinearSegmentedColormap.from_list('my_cmap', my_cmap)
    return my_cmap


my_cmap = make_cmap()


def create_sample_heatmap():
    """
    Creates a sample heat map with a sin-like structure of attractiveness
    :return: The created sample heat map
    """
    global coords_hm, indices_hm
    vals = []
    for x in np.linspace(bottom_left[0], top_right[0], steps):
        row = []
        for y in np.linspace(bottom_left[1], top_right[1], steps):
            row.append(np.sin(x) * np.sin(y))
        vals.append(row)
    hm = np.asarray(vals)
    indices_hm = np.indices(hm.shape)
    indices_hm = np.vstack((indices_hm[0].flatten(), indices_hm[1].flatten())).T
    coords_hm = indices_hm / hm.shape[0] * top_right[1]
    return hm


def create_linear_heatmap():
    """
    Creates a sample heat map with a linear strcture with one corner being most attractive and one being least attractive
    :return: The created heat map
    """
    global coords_hm, indices_hm
    vals = []
    for x in np.linspace(bottom_left[0], top_right[0], steps):
        row = []
        for y in np.linspace(bottom_left[1], top_right[1], steps):
            row.append(x * y)
        vals.append(row)
    hm = np.asarray(vals)
    indices_hm = np.indices(hm.shape)
    indices_hm = np.vstack((indices_hm[0].flatten(), indices_hm[1].flatten())).T
    coords_hm = indices_hm / hm.shape[0] * top_right[1]
    return hm


def load_heatmap(file_loc):
    """
    Loads the heat map csv file located at `file_loc`. Also sets the global parameters `bottom_left, `top_right`, `coords_hm`, and `indices_hm` which are required for the rest of the program to function
    :param file_loc: The string pointing to the csv file containing the heat map
    :return: The created heat map
    """
    global bottom_left, top_right, coords_hm, indices_hm
    stacked = np.genfromtxt(file_loc, delimiter=',')[:, :3]
    full_rectangle = np.stack(np.meshgrid(np.sort(np.unique(stacked[:, 0])), np.sort(np.unique(stacked[:, 1])))).T.reshape(-1, 2)
    full_rectangle = np.hstack((full_rectangle, np.zeros((full_rectangle.shape[0], 1))))
    df_full_rectangle = pd.DataFrame(full_rectangle, columns=['x', 'y', 'value']).set_index(['x', 'y'])
    df_stacked = pd.DataFrame(stacked, columns=['x', 'y', 'value']).set_index(['x', 'y'])
    df_full_rectangle.loc[df_stacked.index, 'value'] = df_stacked['value']
    stacked = df_full_rectangle.reset_index().values
    bottom_left = (stacked[:, 1].min(), stacked[:, 0].min())
    top_right = (stacked[:, 1].max(), stacked[:, 0].max())
    grid_x, grid_y = np.mgrid[
                     stacked[:, 0].min():stacked[:, 0].max():1000j,
                     stacked[:, 1].min():stacked[:, 1].max():1000j
                     ]
    hm = interpolate.griddata(stacked[:, :2], stacked[:, 2], (grid_x, grid_y), method='linear')
    # plt.imshow(hm, extent=(stacked[:, 0].min(), stacked[:, 0].max(), stacked[:, 1].min(), stacked[:, 1].max()),
    #            origin='lower')
    # plt.show()
    coords_hm = np.vstack((grid_y.flatten(), grid_x.flatten())).T
    indices_hm = np.indices(hm.shape)
    indices_hm = np.vstack((indices_hm[0].flatten(), indices_hm[1].flatten())).T
    return hm


def create_sample_stations():
    """
    Randomly selects points for charging stations which serve as a list of existing stations
    :return: An np.ndarray containing the coordinate pairs of the randomly sampled stations
    """
    points = np.array([[np.random.rand() * (top_right[0] - bottom_left[0]) + bottom_left[0],
                        np.random.rand() * (top_right[1] - bottom_left[1]) + bottom_left[1]] for _ in range(10)])
    return points


def load_stations(file_loc: str):
    """
    Loads stations from the list in `file_loc` and filters the ones withing the current window of observation
    :param file_loc: The location of the list of stations
    :return: An np.ndarray containing the coordinate pairs of the stations withing the current viewport
    """
    points = np.genfromtxt(file_loc, delimiter=',')
    mask = (points[:, 0] <= top_right[1]) & (points[:, 0] >= bottom_left[1]) & (points[:, 1] <= top_right[0]) & (
            points[:, 1] >= bottom_left[0])
    points = points[mask, :]
    points = np.fliplr(points)
    return points


def multiprocess_optimizer_wrapper(func, args):
    """
    Unpacks arguments for the function. Used if the multiprocessing function only allows for a single argument per function
    :param func: The function to call
    :param args: The arguments to pass to the function
    :return: The return value of `func`
    """
    return func(*args)


def analyse_city(city: str):
    """
    Runs the full analysis for a city
    :param city: The string name of the city. Must correspond to the heat map file name (excluding the file ending)
    :return: None
    """
    if not os.path.exists(data_path + city):
        os.mkdir(data_path + city)
    heatmap, stations = load_hm_and_sttns(city)
    algorithm_param = {'max_num_iteration': 20,
                       'population_size': 100,
                       'mutation_probability': 0.1,
                       'elit_ratio': 0.01,
                       'crossover_probability': 0.5,
                       'parents_portion': 0.3,
                       'crossover_type': 'uniform',
                       'max_iteration_without_improv': 3}
    function_timeout = 10000
    args = [[run_optimizer_iteratively, (10, algorithm_param, function_timeout, stations, heatmap, True)],
            [run_optimizer_iteratively, (10, algorithm_param, function_timeout, stations, heatmap, False)],
            ]
    results = [multiprocess_optimizer_wrapper(arg[0], arg[1]) for arg in args]
    new_stations_glob, new_stations_ego = results

    # Plot before
    plot_stations_on_hm(heatmap, stations, None, title="Status before", path=f'{city}/')
    # Plot afters
    if not os.path.exists(city):
        os.mkdir(city)
    document_optimization_results(heatmap, new_stations_glob, stations, f"Global optimization in {city}", city, radius_relevant)
    document_optimization_results(heatmap, new_stations_ego, stations, f"Egoistic optimization in {city}", city, radius_relevant)
    print(f"Finished {city}")


def do_results_already_exist(title, city):
    '''
    Check if the results for the given city already exist
    :param title: Custom title and file name used (consistent naming is required for this function to work)
    :param city: The name of the city or district
    :return: True if the results exist for the given title and city
    '''
    return os.path.exists(f"{data_path}{city}/{title}.csv") and \
           os.path.exists(f"{data_path}{city}/{title}.json") and \
           os.path.exists(f"{data_path}{city}/{title}.png")


def document_optimization_results(heatmap, new_stations: np.ndarray, stations, title, city, radius_relevant):
    """
    Takes the result of the optimization and creates result files
    :param heatmap: The array containing the heatmap
    :param new_stations: An array of coordinates with new charging stations
    :param stations: An array with the coordinates already existing charging stations
    :param title: The file name and title to be used in the files
    :param city: The name of the city analysed
    :param radius_relevant: The radius over which attractiveness is covered
    :return: None (results are directly saved to files)
    """
    if len(stations) == 0:
        benefit_before = 0
    else:
        benefit_before = calculate_new_station_benefit(stations[0, :], stations[1:, :], heatmap, True)
    benefit_after = plot_stations_on_hm(heatmap, stations, new_stations, title=title, path=f'{city}/')
    benefits = []
    for i in range(new_stations.shape[0] - 1):
        benefits.append(calculate_new_station_benefit(new_stations[:i+1,:], stations, heatmap, True))
    benefits.append(benefit_after)
    new_stations = np.vstack((np.array([np.nan, np.nan, benefit_before]), np.column_stack((new_stations, np.array(benefits)))))
    np.savetxt(f"{data_path}{city}/{title}.csv", new_stations, delimiter=',')
    result_dict = {
        'Attractiveness before': benefit_before,
        'Attractiveness after': benefit_after,
        'Relative improvement': (benefit_after - benefit_before) / benefit_before,
        'radius': radius_relevant,
    }
    with open(f"{data_path}{city}/{title}.json", 'w+') as f:
        json.dump(result_dict, f)


def load_hm_and_sttns(city):
    """
    Loads the heat map and the already existing stations
    :param city: The name of the city for which to load the heat map
    :return: The loaded heat map and the stations
    """
    hm_path = os.path.expanduser(f'~/Documents/GIS/New_site_suggestor/Heatmaps/{city}.txt')
    sttn_path = os.path.expanduser(f'~/Documents/GIS/New_site_suggestor/locations.csv')
    heatmap = load_heatmap(hm_path)
    # heatmap = create_sample_heatmap()
    # heatmap = create_linear_heatmap()
    stations = load_stations(sttn_path)
    # stations = create_sample_stations()
    return heatmap, stations


def plot_streetmap(ax):
    """
    Plots the open street map tile. Due to limitations of smopy, a bit of a workaround is necessary to correctly position the image
    :param ax: The ax onto which to plot the image
    :return: None
    """
    sp_map = smopy.Map((bottom_left[1], bottom_left[0], top_right[1], top_right[0]), z=11)
    x_low, y_low = sp_map.to_pixels(bottom_left[1], bottom_left[0])
    x_high, y_high = sp_map.to_pixels(top_right[1], top_right[0])
    cropped_img = sp_map.img.crop((x_low, y_high, x_high, y_low))
    ax.imshow(cropped_img, extent=(bottom_left[0], top_right[0], bottom_left[1], top_right[1]))


def plot_stations_on_hm(heatmap, old_stations: np.ndarray, new_stations: np.ndarray, title, path=""):
    '''
    Creates the plots of the stations highlighting newly built and old stations
    :param heatmap: The heat map to plot
    :param old_stations: An array of coordinates of the already existing stations
    :param new_stations: An array of coordinates of the newly added stations
    :param title: The title to plot on each file and to use as file name
    :param path: The path where to store the files
    :return: None
    '''
    global plot_wedges
    plot_wedges_save = plot_wedges
    plot_wedges = True
    if new_stations is None:
        stations = old_stations
    else:
        stations = np.vstack((old_stations, new_stations))

    fig, ax = plt.subplots(1, 1)
    plot_streetmap(ax)
    if len(stations) < 4:
        # There are too few stations in the area. We need to add fake stations since the Voronoi function requires
        # at least 4 points
        stations = np.vstack([np.array(
            [[bottom_left[0] - radius_relevant * 9, bottom_left[1] - radius_relevant * 10],
             [bottom_left[0] - radius_relevant * 15, bottom_left[1] - radius_relevant * 11],
             [bottom_left[0] - radius_relevant * 15, bottom_left[1] - radius_relevant * 15],
             [bottom_left[0] - radius_relevant * 12, bottom_left[1] - radius_relevant * 15]]), stations])
    vor = Voronoi(stations, incremental=True)
    # plt.colorbar()
    ax.imshow(np.flipud(heatmap), cmap=my_cmap, interpolation='nearest',
              extent=[bottom_left[0], top_right[0], bottom_left[1], top_right[1]])  # , alpha=0.3)
    voronoi_plot_2d(vor, ax, show_vertices=False, show_points=False)
    benefit_after = calculate_new_station_benefit(stations[0, :], stations[1:, :], heatmap, True)
    margin = 0
    plt.ylim((bottom_left[1] - margin, top_right[1] + margin))
    plt.xlim((bottom_left[0] - margin, top_right[0] + margin))
    ax.scatter(old_stations[:, 0], old_stations[:, 1], c='g', label='Existing stations')
    if new_stations is not None:
        if new_stations.ndim == 1:
            ax.scatter(new_stations[0], new_stations[1], c='b', label='Added stations')
        else:
            ax.scatter(new_stations[:, 0], new_stations[:, 1], c='b', label='Added stations')
            for i in range(new_stations.shape[0]):
                ax.annotate(i + 1, new_stations[i, :])
    ax.legend()
    if title is not None:
        ax.set_title(title)
    plt.savefig(f"{data_path}{path}{title}.svg")
    plt.savefig(f"{data_path}{path}{title}.pdf")
    plt.savefig(f"{data_path}{path}{title}.png")
    # plt.show()
    plot_wedges = plot_wedges_save
    return benefit_after


def run_optimizer_iteratively(num_iters, algorithm_param, function_timeout, stations, heatmap,
                              optimize_globally):
    """
    Runs the optimizer `num_iters` times
    :param num_iters: The number of iterations (i.e. the number of new stations)
    :param algorithm_param: The parameters to be used for the genetic optimizer
    :param function_timeout: Timout for calculating the station benefit
    :param stations: The list of stations that already exist
    :param heatmap: The heat map to place the new station on
    :param optimize_globally: `True` if the optimizer should use the covered benefit of all stations and `False` if only the benefit of the newly added station is relevant
    :return: A list of new stations
    """
    new_stations = []
    if stations.shape[0] == 0:
        initial_attractiveness = 0
    else:
        initial_attractiveness = calculate_new_station_benefit(stations[0, :], stations[1:, :], heatmap, True)
    for i in range(num_iters):
        new_station = run_optimizer(algorithm_param, function_timeout, stations, heatmap, optimize_globally)
        new_stations.append(new_station)
        stations = np.vstack((new_station, stations))
    new_stations = np.vstack(new_stations)
    final_attractiveness = calculate_new_station_benefit(stations[0, :], stations[1:, :], heatmap, True)
    print(
        f"Ran the optimizer {num_iters} times consecutively with {'global' if optimize_globally else 'egoistic'} "
        f"approach. New stations were added in the following order:")
    print(new_stations)
    print(
        f"Attractiveness changed globally from {initial_attractiveness} to {final_attractiveness} which is a change of "
        f"{100 * (final_attractiveness - initial_attractiveness) / initial_attractiveness} %")
    return new_stations


def run_optimizer(algorithm_param, function_timeout, stations, heatmap, optimize_globally, num_stations=1):
    """
    Runs the genetic optimizer
    :param algorithm_param: The parameters given to the optimizer
    :param function_timeout: Timout for the calculation of covered benefit
    :param stations: An array of existing stations
    :param heatmap: The heat map to place new stations on
    :param optimize_globally: `True` if the optimizer should use the covered benefit of all stations and `False` if only the benefit of the newly added station is relevant
    :param num_stations: The number of stations to place in a single optimization. It is recommended to only place one station since the iteraction between two new stations is challenging for the genetic optimizer to capture
    :return: The result of the optimization which are charging station locations
    """
    varbound = np.array([[bottom_left[0], top_right[0]],
                         [bottom_left[1], top_right[1]]])
    if num_stations > 1:
        varbound = np.tile(varbound, (num_stations, 1))

    def opt_func_global(x):
        return -calculate_new_station_benefit(x, stations, heatmap, optimize_globally)

    model = ga(function=opt_func_global,
               dimension=2 * num_stations,
               variable_type='real',
               variable_boundaries=varbound,
               algorithm_parameters=algorithm_param,
               function_timeout=function_timeout,
               progress_bar=False,
               convergence_curve=False)
    model.run()
    best_variable = model.best_variable
    if num_stations > 1:
        best_variable = np.reshape(best_variable, (-1, 2))
    return best_variable


def calculate_new_station_benefit(new_station_loc: np.ndarray, existing_stations, heatmap, global_optimization):
    """
    Calculates the captured benefit of a new charging station
    :param new_station_loc: Coordinates of the new station
    :param existing_stations: An array of coordinates of already existing stations
    :param heatmap: The heat map on which the station is placed
    :param global_optimization: `True` if the optimizer should use the covered benefit of all stations and `False` if only the benefit of the newly added station is relevant
    :return: The calculated new benefit
    """
    if new_station_loc.size > 2:
        new_station_loc = np.reshape(new_station_loc, (-1, 2))
    if new_station_loc.ndim == 1:
        if any((existing_stations[:] == new_station_loc).all(1)):
            # Building a station at the exact same location as an existing one is not allowed
            return -10 ** 20
    else:
        if (new_station_loc[:, None] == existing_stations).all(-1).any(-1).any():
            # Building a station at the exact same location as an existing one is not allowed
            return -10 ** 20
    if len(existing_stations) < 4:
        # There are too few stations in the area. We need to add fake stations since the Voronoi function requires
        # at least 4 points
        existing_stations = np.vstack([np.array(
            [[bottom_left[0] - radius_relevant * 10, bottom_left[1] - radius_relevant * 10],
             [bottom_left[0] - radius_relevant * 15, bottom_left[1] - radius_relevant * 10],
             [bottom_left[0] - radius_relevant * 15, bottom_left[1] - radius_relevant * 15]]), existing_stations])
        vor = Voronoi(np.vstack((new_station_loc, existing_stations)))
    else:
        if global_optimization:
            vor = Voronoi(np.vstack((new_station_loc, existing_stations)))
        else:
            search_radius = radius_relevant
            if new_station_loc.ndim == 1:
                search_new_stations = new_station_loc[np.newaxis, :]
            else:
                search_new_stations = new_station_loc
            while True:
                # This is a workaround since Voronoi fails if it has less than 4 points to work with.
                # This can be replaced with custom logic for the cases where less than three other stations are within range
                relevant_other_stations = []
                for station in existing_stations:
                    for new_station in search_new_stations:
                        if np.linalg.norm(station - new_station) <= 2 * search_radius:
                            relevant_other_stations.append(station)
                            break
                relevant_other_stations = np.array(relevant_other_stations)
                if len(np.unique(relevant_other_stations, axis=0)) >= 3:
                    break
                search_radius += radius_relevant
                if search_radius > np.sqrt((top_right[0] - bottom_left[0]) ** 2 + (top_right[1] - bottom_left[1]) ** 2):
                    raise ValueError("There are too few other charging stations in the selected area.")
            vor = Voronoi(np.vstack((new_station_loc, relevant_other_stations)))
    if debug_mode:
        fig, ax = plt.subplots(1, 1)
        ax.imshow(np.flipud(heatmap), cmap='hot', interpolation='nearest',
                  extent=[bottom_left[0], top_right[0], bottom_left[1], top_right[1]])
        voronoi_plot_2d(vor, ax)
    if global_optimization:
        result = sum(calculate_area_integrals(vor, heatmap, None))
    else:
        if new_station_loc.ndim == 1:
            result = calculate_area_integrals(vor, heatmap, 0)
        else:
            result = calculate_area_integrals(vor, heatmap, [x for x in range(new_station_loc.shape[0])])
    if debug_mode:
        margin = 0
        plt.ylim((bottom_left[1] - margin, top_right[1] + margin))
        plt.xlim((bottom_left[0] - margin, top_right[0] + margin))
        plt.show()
    return result


def calculate_area_integrals(vor, sample_heatmap, index_of_interest: int = None):
    """
    Calculate an area integral of all points or the point pointed at through `index_of_interest` in `vor`
    :param vor: The voronoy result
    :param sample_heatmap: The heat map used to calculate the area integral
    :param index_of_interest: The index in `vor` of which the benefit shall be calculated. If `None`, the captured of all points in `vor` is summed
    :return: The calculated area integral
    """
    center = vor.points.mean(axis=0)
    if index_of_interest is None:
        point_area_integrals = []
        for i_pt in range(len(vor.points)):
            point_area_integral = area_integral_single_point(i_pt, vor, sample_heatmap, center, coords_hm, indices_hm)
            point_area_integrals.append(point_area_integral)
        return point_area_integrals
    else:
        if isinstance(index_of_interest, int):
            return area_integral_single_point(index_of_interest, vor, sample_heatmap, center, coords_hm, indices_hm)
        else:
            return sum([area_integral_single_point(x, vor, sample_heatmap, center, coords_hm, indices_hm) for x in
                        index_of_interest])


def area_integral_single_point(i_pt, vor, heatmap, center, coords_hm, indices_hm):
    '''
    Calculates the area integral around a single point. This corresponds to the attractiveness that a charging station is able to capture.
    :param i_pt: The index of the point to calculate the area integral
    :param vor: The voronoy result containing a list of points
    :param heatmap: The heat map to calculate the area integral on
    :param center: The center coordinates of the current plot
    :param coords_hm: An array of coordinates of the heat map
    :param indices_hm: An array of indices of the heat map
    :return: Calculated area integral value
    '''
    segments, infinite_segments = get_ridges_for_point_index(i_pt, vor, center)
    pt = vor.points[i_pt]
    coords_hm, indices_hm = reduce_hm_to_relevant_area(pt, coords_hm, indices_hm)
    contribution = 0
    for segment in segments:
        if segment[0][0] > segment[1][0]:
            segment = [segment[1], segment[0]]
        pt_a_in_range = np.linalg.norm(segment[0] - pt) <= radius_relevant
        pt_b_in_range = np.linalg.norm(segment[1] - pt) <= radius_relevant
        if pt_a_in_range and pt_b_in_range:
            contribution_sub = area_integral_both_vertices_in_range(pt, segment, heatmap, coords_hm,
                                                                    indices_hm)
        elif pt_a_in_range or pt_b_in_range:
            contribution_sub = area_integral_one_vertice_in_range(pt, segment, heatmap, coords_hm,
                                                                  indices_hm, pt_a_in_range)
        else:
            contribution_sub = area_integral_both_vertices_outside_range(pt, segment, heatmap, coords_hm,
                                                                         indices_hm)
        contribution += contribution_sub
        if debug_mode:
            print(
                f"Added {np.round(contribution_sub, 1)} to the contribution table in segment {segment} to current "
                f"running total of {contribution}")
    if len(infinite_segments) == 2:
        angle1 = get_angle_between_points(pt, infinite_segments[0][1])
        angle2 = get_angle_between_points(pt, infinite_segments[1][1])
        contribution += area_integrate_between_angles(pt, angle1, angle2, heatmap, coords_hm,
                                                      indices_hm)
    if contribution == 0:
        return 0
    nearest_idx = indices_hm[np.sum(np.abs(coords_hm - pt), axis=1).argmin()]
    pt_value = heatmap[nearest_idx[0], nearest_idx[1]]
    # pt_value = interp2d(coords_hm[:, 0], coords_hm[:, 1], heatmap[indices_hm[:, 0], indices_hm[:, 1]])(pt[0], pt[1])
    point_area_integral = contribution / heatmap[indices_hm[:, 0], indices_hm[:, 1]].sum() * pt_value  # / (heatmap.shape[0] / end) ** 2
    return point_area_integral


def reduce_hm_to_relevant_area(pt, coords_hm, indices_hm):
    """
    Since the area integral is limited to a small range, the the relevant coordinates of the heat map can be reduced substantially
    :param pt: The point around which to reduce the heat map
    :param coords_hm: The unmodified coordinate map of the heat map
    :param indices_hm: The unmodified coordinate map of the heat map
    :return: The reduced versions of `coords_hm` and `indices_hm`
    """
    epsilon = 0.01
    relevant_hm_idx = (coords_hm[:, 0] >= (pt[0] - radius_relevant - epsilon)) & \
                      (coords_hm[:, 0] <= (pt[0] + radius_relevant + epsilon)) & \
                      (coords_hm[:, 1] >= (pt[1] - radius_relevant - epsilon)) & \
                      (coords_hm[:, 1] <= (pt[1] + radius_relevant + epsilon))
    coords_hm = coords_hm[relevant_hm_idx, :]
    indices_hm = indices_hm[relevant_hm_idx, :]
    circle = mpatches.Circle(pt, radius_relevant)
    # The below is necessary since a circle is constructed from the unit circle and transform
    relevant_hm_idx = circle.get_transform().transform_path(circle.get_path()).contains_points(coords_hm)
    coords_hm = coords_hm[relevant_hm_idx, :]
    indices_hm = indices_hm[relevant_hm_idx, :]
    return coords_hm, indices_hm


def area_integral_both_vertices_outside_range(pt, segment, heatmap, coords_hm, indices_hm):
    """
    Calculate the area integral if both vertices of the area integral are outside the range in which a station is able to capture attractiveness
    :param pt: The point around which to calculate the area integral
    :param segment: The line segment closes to the point
    :param heatmap: The heat map on which the area integral is calculated
    :param coords_hm: The map of coordinates of the heat map
    :param indices_hm: The map of indices of the heat map
    :return: The contribution of this segment
    """
    contribution_sub = 0
    closest_point_on_line = find_closest_point_on_line(segment, pt)
    closest_point_in_range = np.linalg.norm(pt - closest_point_on_line) <= radius_relevant and \
                             segment[0][0] <= closest_point_on_line[0] <= segment[1][0]
    if closest_point_in_range:
        m, c = lin_equ(segment[0], segment[1])
        intersects = get_line_circle_intersect(m, c, pt, radius_relevant)
        contribution_sub += area_integration_polygon(np.vstack((intersects, pt)), heatmap, coords_hm,
                                                     indices_hm)
        angle_seg0 = get_angle_between_points(pt, segment[0])
        angle_seg1 = get_angle_between_points(pt, segment[1])
        angle_intersec0 = get_angle_between_points(pt, intersects[0])
        angle_intersec1 = get_angle_between_points(pt, intersects[1])
        if abs((angle_seg0 - angle_intersec0 + 180) % 360 - 180) < abs(
                (angle_seg0 - angle_intersec1 + 180) % 360 - 180):
            contribution_sub += area_integrate_between_angles(pt, angle_seg0, angle_intersec0, heatmap,
                                                              coords_hm,
                                                              indices_hm)
            contribution_sub += area_integrate_between_angles(pt, angle_seg1, angle_intersec1, heatmap,
                                                              coords_hm,
                                                              indices_hm)
        else:
            contribution_sub += area_integrate_between_angles(pt, angle_seg1, angle_intersec0, heatmap,
                                                              coords_hm,
                                                              indices_hm)
            contribution_sub += area_integrate_between_angles(pt, angle_seg0, angle_intersec1, heatmap,
                                                              coords_hm,
                                                              indices_hm)
    else:
        angle1 = get_angle_between_points(pt, segment[0])
        angle2 = get_angle_between_points(pt, segment[1])
        contribution_sub += area_integrate_between_angles(pt, angle1, angle2, heatmap, coords_hm,
                                                          indices_hm)
    return contribution_sub


def area_integral_one_vertice_in_range(pt, segment, heatmap, coords_hm, indices_hm, pt_a_in_range):
    """
    Calculate the area integral of one if the `segment`s vertices is within the range in which the station is able to capture attractiveness
    :param pt: The point for which the area integral is calculated
    :param segment: The line segment closest to the `pt`
    :param heatmap: The heat map on which to calculate the area integral
    :param coords_hm: The coordinates belonging to `heatmap`
    :param indices_hm: The indices belonging to `indices_hm`
    :param pt_a_in_range: `True` if pt a of the segment is within the range and `False` if pt b of the segment is within range
    :return: The resulting area integral value
    """
    contribution_sub = 0
    # The following is necessary in case the differences between x-values are very small which leads to excessively
    # large c and m values and causes an error due to inaccuracies.
    segment[0] = np.around(segment[0], decimals=8)
    segment[1] = np.around(segment[1], decimals=8)
    m, c = lin_equ(segment[0], segment[1])
    intersects = get_line_circle_intersect(m, c, pt, radius_relevant)
    if intersects[0] == intersects[1]:
        return 0
    if segment[0][0] <= intersects[0][0] <= segment[1][0]:
        intersect = intersects[0]
    elif segment[0][0] <= intersects[1][0] <= segment[1][0]:
        intersect = intersects[1]
    else:
        print(f'None of {intersects} lie between {segment}')
        return 0
    if pt_a_in_range:
        path_vertices = np.vstack((pt, segment[0], intersect))
    else:
        path_vertices = np.vstack((pt, segment[1], intersect))
    contribution_sub += area_integration_polygon(path_vertices, heatmap, coords_hm, indices_hm)
    if pt_a_in_range:
        angle2 = get_angle_between_points(pt, intersect)
        angle1 = get_angle_between_points(pt, segment[1])
    else:
        angle1 = get_angle_between_points(pt, intersect)
        angle2 = get_angle_between_points(pt, segment[0])
    contribution_sub += area_integrate_between_angles(pt, angle1, angle2, heatmap, coords_hm,
                                                      indices_hm)
    return contribution_sub


def area_integral_both_vertices_in_range(pt, segment, heatmap, coords_hm, indices_hm):
    """
    Area integral if both vertices of the segment are within the range where a station is able to capture attractiveness
    :param pt: The point around which the area integral should be calculated
    :param segment: The segment along which the area integral is calculated
    :param heatmap: The heat map on which to calculate the area integral
    :param coords_hm: The coordinates belonging to `heatmap`
    :param indices_hm: The indices belonging to `indices_hm`
    :return: The resulting area integral value
    """
    path_vertices = np.vstack((segment, pt))
    contribution_sub = area_integration_polygon(path_vertices, heatmap, coords_hm, indices_hm)
    return contribution_sub


def area_integrate_between_angles(pt, angle1, angle2, heatmap, coords_hm, indices_hm):
    """
    Calculate the area integral of the circle selgment defined by àngle1` and `angle2` around `pt`
    :param pt: The point around which the area integral is calculated
    :param angle1: The first angle defining the circle segment
    :param angle2: The second angle defining the circle segment
    :param heatmap: The heat map on which to calculate the area integral
    :param coords_hm: The coordinates belonging to `heatmap`
    :param indices_hm: The indices belonging to `indices_hm`
    :return: The area integral of the circle segment
    """
    if angle1 > angle2:
        angle1, angle2 = angle2, angle1
    if angle2 - angle1 > 180:
        angle1, angle2 = angle2, angle1
    contribution_sub = area_integration_wedge(pt, radius_relevant, angle1, angle2, heatmap=heatmap,
                                              coords_hm=coords_hm, indices_hm=indices_hm)
    return contribution_sub


def find_closest_point_on_line(segment, centre_point):
    """
    Find the point on `segment` closest to `centre_point`
    :param segment: A line segment
    :param centre_point: A point
    :return: The coordinates of the point on `segment` that is closest to `centre_point`
    """
    x1, y1 = segment[0]
    x2, y2 = segment[1]
    x3, y3 = centre_point
    dx, dy = x2 - x1, y2 - y1
    det = dx * dx + dy * dy
    a = (dy * (y3 - y1) + dx * (x3 - x1)) / det
    return x1 + a * dx, y1 + a * dy


def get_wedge_path(center, radius, theta1, theta2):
    """
    Creates a wedge path from the circle segment definition given the parameters
    :param center: The centre of the circle
    :param radius: The radius of the circle
    :param theta1: Angle one defining the circle segment
    :param theta2: Angle two defining the circle segment
    :return: The wedge path created
    """
    circ = mpatches.Wedge(center, radius, theta1, theta2, fill=True)
    return circ.get_path()


def angle_trunc(a):
    """
    Ensure a positve value of angle a
    :param a: The angle in radians which may be between -2*pi and 0
    :return: The angle a expressed as only positive value
    """
    while a < 0.0:
        a += math.pi * 2
    return a


def get_angle_between_points(from_pt, to_pt):
    """
    Calculates the angle between two points
    :param from_pt: Point 1
    :param to_pt: Point 2
    :return: The angle of the vector between `from_pt` and `to_pt`
    """
    delta_y = to_pt[1] - from_pt[1]
    delta_x = to_pt[0] - from_pt[0]
    return math.degrees(angle_trunc(math.atan2(delta_y, delta_x)))


def area_integration_polygon(path_vertices, heatmap, coords_hm, indices_hm):
    """
    Calculate the area integral of a polygon defined by `path_vertices`
    :param path_vertices: The vertices of the polygoon
    :param heatmap: The heat map on which to calculate the area integral
    :param coords_hm: The coordinates belonging to `heatmap`
    :param indices_hm: The indices belonging to `indices_hm`
    :return: The area integral of the polygon
    """
    path_polygon = path.Path(path_vertices)
    return area_integration_from_path(path_polygon, heatmap, coords_hm, indices_hm)


def area_integration_from_path(path_to_integrate, heatmap, coords_hm, indices_hm):
    """
    Calculate the area integral from the path to integrate
    :param path_to_integrate: Path to integrate
    :param heatmap: The heat map on which to calculate the area integral
    :param coords_hm: The coordinates belonging `to heatmap`
    :param indices_hm: The indices belonging to `indices_hm`
    :return: The area integral of the path
    """
    contained_pts = path_to_integrate.contains_points(coords_hm)
    selected_idx = indices_hm[contained_pts]
    contribution_sub = np.nansum(heatmap[selected_idx[:, 0], selected_idx[:, 1]])
    if debug_mode or plot_wedges:
        patch = mpatches.PathPatch(path_to_integrate, facecolor='green', linewidth=0, alpha=0.3)
        plt.gca().add_patch(patch)
    return contribution_sub


def area_integration_wedge(center, radius, theta1, theta2, heatmap, coords_hm, indices_hm):
    """
    Calculate the area integral of a circle wedge
    :param center: The centre of the circle to integrate over
    :param radius: The radius of the circle
    :param theta1: The angle towards the first corner of the wedge
    :param theta2: The angle towards the second corner of the wedge
    :param heatmap: The heat map on which to calculate the area integral
    :param coords_hm: The coordinates belonging to `heatmap`
    :param indices_hm: The indices belonging to `indices_hm`
    :return: The area integral of the wedge
    """
    path_wedge = get_wedge_path(center, radius, theta1, theta2)
    return area_integration_from_path(path_wedge, heatmap, coords_hm, indices_hm)


def get_line_circle_intersect(line_m, line_c, circle_midpoint, circle_radius):
    """
    Calculate the coordinates of the intersection of the line and circle
    :param line_m: Gradient of the line
    :param line_c: y-axis intercept of the line. If `np.isinf`, it is assumed that the line is vertical and that `line_m` contains the x-coordinate of vertical line
    :param circle_midpoint: The center of the circle
    :param circle_radius: The radius of the circle
    :return: The coordinates of the intersections
    """
    if np.isinf(line_c):
        # Special case where the line is vertical
        sqrt = np.sqrt(circle_radius ** 2 - (line_m - circle_midpoint[0]) ** 2)
        y1 = circle_midpoint[1] + sqrt
        y2 = circle_midpoint[1] - sqrt
        return (line_m, y1), (line_m, y2)
    # Constructed after https://www.wolframalpha.com/input/?i=%28x-x0%29%5E2%2B%28%28ax%2Bb%29-y0%29%5E2%3DR%5E2
    sqrt = np.sqrt(line_m ** 2 * circle_radius ** 2
                   - line_m ** 2 * circle_midpoint[0] ** 2
                   - 2 * line_m * line_c * circle_midpoint[0]
                   + 2 * line_m * circle_midpoint[0] * circle_midpoint[1]
                   - line_c ** 2
                   + 2 * line_c * circle_midpoint[1]
                   + circle_radius ** 2
                   - circle_midpoint[1] ** 2)
    if not np.isreal(sqrt):
        raise ValueError(
            f"Circle and line defined by m={line_m}, c={line_c}, midpoint={circle_midpoint}, radius={circle_radius} do not intersect!")
    nom2 = -line_m * line_c + line_m * circle_midpoint[1] + circle_midpoint[0]
    denom = line_m ** 2 + 1
    x1 = (-sqrt + nom2) / denom
    x2 = (sqrt + nom2) / denom
    y1 = line_m * x1 + line_c
    y2 = line_m * x2 + line_c
    return (x1, y1), (x2, y2)


def lin_equ(l1, l2):
    """
    Line encoded as l=(x,y).
    :param l1: Point 1 on the line
    :param l2: Point 2 on the line
    :return: The line parametrised as by gradient and y-intersection
    """
    if l2[0] == l1[0]: # Catch the case where both have the same x-coordinate
        return l1[0], np.inf
    m = (l2[1] - l1[1]) / (l2[0] - l1[0])
    c = (l2[1] - (m * l2[0]))
    return m, c


def get_ridges_for_point_index(i_pt, vor, center):
    '''
    This function finds the ridges corresponding to the point at index i in vor
    :param i_pt: The index of the point
    :param vor: The voronoy result contianing the points of interest
    :param center: The centre point of the voronoi plot
    :return: The ridges corresponding to `i_pt`
    '''
    pt = vor.points[i_pt]
    # Following code adapted from voronoi_plot_2d
    finite_segments = []
    infinite_segments = []
    point_ridges_idx = [i_ridge for i_ridge in range(len(vor.ridge_points)) if i_pt in vor.ridge_points[i_ridge]]
    for pointidx, simplex in zip([vor.ridge_points[i_ridge] for i_ridge in point_ridges_idx],
                                 [vor.ridge_vertices[i_ridge] for i_ridge in point_ridges_idx]):
        simplex = np.asarray(simplex)
        if np.all(simplex >= 0):
            finite_segments.append(vor.vertices[simplex])
        else:
            i = simplex[simplex >= 0][0]  # finite end Voronoi vertex

            t = vor.points[pointidx[1]] - vor.points[pointidx[0]]  # tangent
            t /= np.linalg.norm(t)
            n = np.array([-t[1], t[0]])  # normal

            midpoint = vor.points[pointidx].mean(axis=0)
            direction = np.sign(np.dot(midpoint - center, n)) * n
            if vor.furthest_site:
                direction = -direction
            far_point = vor.vertices[i] + direction * np.sqrt(
                (top_right[0] - bottom_left[0]) ** 2 + (top_right[1] - bottom_left[1]) ** 2) * 100

            infinite_segments.append([vor.vertices[i], far_point])
    segments = infinite_segments + finite_segments
    return segments, infinite_segments


def plot_existing(file: str):
    '''
    Helper function that generates a station plot if the results already exist in text form
    :param file: The file where the results are contained
    :return: None
    '''
    heatmap, stations = load_hm_and_sttns()
    new_stations = np.genfromtxt(file, delimiter=',')
    plot_stations_on_hm(heatmap, stations, new_stations, title=file.replace('.csv', ''))


if __name__ == '__main__':
    os.chdir(os.path.expanduser("~/Documents/GIS/New_site_suggestor/Heatmaps"))  # Modify directory as needed for your purposes
    with multiprocessing.Pool(multiprocessing.cpu_count()*2) as pool:
        args = [file.replace('.txt', '') for file in glob.glob("*.txt")]
        # for arg in args:
        #     analyse_city(arg)
        # args = [city for city in args if not (do_results_already_exist(f"Global optimization in {city}", city) and do_results_already_exist(f"Egoistic optimization in {city}", city))]
        for _ in pool.imap_unordered(analyse_city, args):
            pass

# Expansion Planning of Public Charging Infrastructure

Data and code repo for the paper "Optimized Expansion Planning of Public Charging Infrastructure for Electric Vehicles Based on Real-World Charging Data and Local Points of Interest"